<?php

use App\Http\Controllers\PostController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::get('/post', '\App\Http\Controllers\PostController@index');
// Route::get('/post', '\App\Http\Controllers\PostController@first80');

Route::get('/data80', function () {

    $first80 = DB::select('SELECT * FROM data p1 WHERE
                         (SELECT count(*) FROM data p2 WHERE p2.tgl >= p1.tgl) >
                         (SELECT 0.2 * count(*) FROM data)');

    $last20 = DB::select('SELECT * FROM data p1 WHERE
                        (SELECT count(*) FROM data p2 WHERE p2.tgl >= p1.tgl) <=
                        (SELECT 0.2 * count(*) FROM data)');

    return $first80;
    // return $last20;
});

Route::get('/data20', function () {

    $last20 = DB::select('SELECT * FROM data p1 WHERE
                        (SELECT count(*) FROM data p2 WHERE p2.tgl >= p1.tgl) <=
                        (SELECT 0.2 * count(*) FROM data)');

    // return $first80;
    return $last20;
});
